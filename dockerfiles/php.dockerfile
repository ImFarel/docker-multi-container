FROM php:8.2-fpm-alpine

WORKDIR /var/www

COPY source/backend backend
COPY source/static html

RUN docker-php-ext-install pdo pdo_mysql

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

# UNCOMMENT THIS WHEN NOT USING BIND MOUNT
# RUN chown -R laravel:laravel .

USER laravel