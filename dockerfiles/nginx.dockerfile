FROM nginx:stable-alpine

WORKDIR /etc/nginx/conf.d

COPY config/nginx/nginx.conf .

RUN mv nginx.conf default.conf

WORKDIR /var/www

RUN mkdir backend

COPY source/backend backend
COPY source/static html